# fug conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/fug"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS fug module
